package com.example.s04proj03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Dish dish1 = new Dish("");
        System.out.println("Dish 1: " + dish1);
        Dish dish2 = new Dish("");
        System.out.println("Dish 2: " + dish2);
        Dish dish3 = dish1;
        System.out.println("Dish 3: " + dish3);

        System.out.println("dish1 == dish2 " + dish1.equals(dish2));
        System.out.println("dish1 == dish3 " + dish1.equals(dish3));
        System.out.println("dish2 == dish3 " + dish2.equals(dish3));

        Menu menu = new Menu();
        HashMap<String, ArrayList<Dish>> dishesByCuisine = menu.dishesByCuisine();
    }
}