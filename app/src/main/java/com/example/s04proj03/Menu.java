package com.example.s04proj03;

import java.util.ArrayList;
import java.util.HashMap;

public class Menu {

    // Public method
    public HashMap<String, ArrayList<Dish>> dishesByCuisine() {
        if (menu == null) {
            populateMenu();
        }
        return menu;
    }

    // Private property
    private HashMap<String, ArrayList<Dish>> menu = null;  // "CuisineName" : [ Dish }

    // Private method
    private void populateMenu() {
        menu = new HashMap<String, ArrayList<Dish>>();

        // Iterate through every possible Cuisine type
        for (Cuisine cuisine : Cuisine.values()) {
            ArrayList<Dish> dishesList = new ArrayList<Dish>();

            // Insert 6 dishes for each cuisine
            for (int i = 0; i < 6; i++) {
                Dish dish = new Dish("");
                dishesList.add(dish);
            }

            // Here, dishesList will have 6 dishes added to it
            // So add it to the menu dictionary
            menu.put(cuisine.toString(), dishesList);
        }

        // Here, my menu dictionary should be all populated
        System.out.println("Menu: " + menu);
    }
}
